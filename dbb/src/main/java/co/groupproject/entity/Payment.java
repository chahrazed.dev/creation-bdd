package co.groupproject.entity;

public class Payment {
    private int id;
    private String typeOfPaiment;
    private String printBill;
    
    public Payment(String typeOfPaiment, String printBill) {
        this.typeOfPaiment = typeOfPaiment;
        this.printBill = printBill;
    }

    public Payment(int id, String typeOfPaiment, String printBill) {
        this.id = id;
        this.typeOfPaiment = typeOfPaiment;
        this.printBill = printBill;
    }

    public Payment() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTypeOfPaiment() {
        return typeOfPaiment;
    }

    public void setTypeOfPaiment(String typeOfPaiment) {
        this.typeOfPaiment = typeOfPaiment;
    }

    public String getPrintBill() {
        return printBill;
    }

    public void setPrintBill(String printBill) {
        this.printBill = printBill;
    }
}

