package co.groupproject;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Database {
    public static Connection connect() throws SQLException {
        Properties props = new Properties();
        try {
            props.load(Database.class.getClassLoader().getResourceAsStream("application.properties"));

        } catch (IOException e) {
            System.out.println("Propertie file loading error");
            e.printStackTrace();
        }
        return DriverManager.getConnection(props.getProperty("database.url"));
    }
}

