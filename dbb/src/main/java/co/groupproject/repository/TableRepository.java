package co.groupproject.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import co.groupproject.Database;
import co.groupproject.entity.Table;

public class TableRepository {

    public List<Table> findAll() {
        List<Table> list = new ArrayList<>();

        try (Connection connection = Database.connect()) {

            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM table_resto");

            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Table table = new Table(
                    result.getInt("id"),
                    result.getInt("table_number"),
                    result.getInt("capacity")
                );
                list.add(table);

            }

        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }

        return list;
    }

    public Table findById(int id) {

        try (Connection connection = Database.connect()) {

            PreparedStatement stmt = 
            connection.prepareStatement("SELECT * FROM table_resto WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            if (result.next()) {
                return new Table(
                    result.getInt("id"),
                    result.getInt("table_number"),
                    result.getInt("capacity")
                );
            }

        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }

        return null;
    }

    public boolean updateTable(Table table){

    try(Connection connection = Database.connect()) {
    PreparedStatement stmt = connection.prepareStatement("UPDATE table_resto SET table_number=?,capacity=? WHERE id=?");
        stmt.setInt(1, table.getTableNumber());
        stmt.setInt(2, table.getCapacity());
        stmt.setInt(3, table.getId());
        
        if (stmt.executeUpdate() == 1) {
            return true;
        }


    } catch (SQLException e) {
        System.out.println("Error from repository");
        e.printStackTrace();
    }
    
    return false;

     }

     public boolean persistTable(Table table) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO table_resto (table_number,capacity) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setInt(1, table.getTableNumber());
            stmt.setInt(2, table.getCapacity());
            
            if(stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                table.setId(keys.getInt(1));
                return true;
            }

            
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }
        return false;
    }

    public boolean deleteTable(int id) {

        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM table_resto WHERE id=?");
            stmt.setInt(1, id);
            if(stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }

        return false;
    }
}
