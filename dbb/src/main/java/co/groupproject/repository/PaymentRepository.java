package co.groupproject.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BooleanSupplier;

import co.groupproject.Database;
import co.groupproject.entity.Payment;

public class PaymentRepository {

    public List<Payment> findAll() {
        List<Payment> list = new ArrayList<>();

        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM paiement");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Payment paiement = new Payment(
                    result.getInt("id"),
                    result.getString("typeOfPaiement"),
                    result.getString("printBill")
                );
                list.add(paiement);
            }

        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }
        return list;
    }

    public Payment findById(int id) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM paiement WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            if (result.next()) {
                return new Payment(
                    result.getInt("id"),
                    result.getString("typeOfPaiement"),
                    result.getString("printBill")
                );
            }

        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }
        return null;
    }

    public boolean updatePaiement(Payment paiement) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE paiement SET typeOfPaiement= ?, printBill=? WHERE id=?");
            stmt.setString(1, paiement.getTypeOfPaiment());
            stmt.setString(2, paiement.getPrintBill());
            stmt.setInt(3, paiement.getId());

            if (stmt.executeUpdate() == 1) {
                
                System.out.println("reussi");
                return true;
                
            }

        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }
        System.out.println("raté");
        return false;

    }

    public boolean persistPayment(Payment paiement) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO paiement (typeOfPaiement,printBill) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, paiement.getTypeOfPaiment());
            stmt.setString(2, paiement.getPrintBill());

            if (stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                paiement.setId(keys.getInt(1));
                return true;
            }

        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }
        return false;
    }

    public boolean deletePaiement(int id) {

        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM paiement WHERE id=?");
            stmt.setInt(1, id);
            if(stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }

        return false;
    }

    public BooleanSupplier update(Payment paiement) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'update'");
    }
}
