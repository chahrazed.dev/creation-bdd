package co.groupproject.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import co.groupproject.Database;
import co.groupproject.Menu;

public class MenuRepository {
    public List<Menu> findAllitem() {
        List<Menu> list = new ArrayList<>();

        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM menu");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Menu Menu = new Menu(
                        result.getString("item"),
                        result.getDouble("price")
                        );

                list.add(Menu);

            }

        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }

        return list;
    }

    public Menu findItemById(int id) {

        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = 
            connection.prepareStatement("SELECT * FROM menu WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            if (result.next()) {
                return new Menu(
                    result.getString("item"),
                    result.getDouble("price")
                    );
            }

        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }

        return null;
    }

    public boolean deleteItem(int id) {

        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM menu WHERE id=?");
            stmt.setInt(1, id);
            if(stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }

        return false;
    }


    public boolean persistItem(Menu menu) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO menu (item, price) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, menu.getItem());
            stmt.setDouble(2,menu.getPrice());
            
            
            if(stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                menu.setId(keys.getInt(1));
                return true;
            }

            
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }
        return false;
    }





     public boolean updateMenu(Menu menu){

    try(Connection connection = Database.connect()) {
    PreparedStatement stmt = connection.prepareStatement("UPDATE menu SET item=?,price=? WHERE id=?");
    stmt.setString(1, menu.getItem());
    stmt.setDouble(2,menu.getPrice());
    

        return stmt.executeUpdate() == 1;


    } catch (SQLException e) {
        System.out.println("Error from repository");
        e.printStackTrace();
    }

        return false;

     }

    public boolean updateItemPrice(int id, double newPrice) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE menu SET price=? WHERE id=?");
            stmt.setDouble(1, newPrice);
            stmt.setInt(2, id);

            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }

        return false;
    }

    public boolean isItemAvailable(int id) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT available FROM menu WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            if (result.next()) {
                return result.getBoolean("available");
            }

        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }

        return false;
    }

    

    public List<Menu> searchItems(String keyword) {
        List<Menu> list = new ArrayList<>();

        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM menu WHERE item LIKE ?");
            stmt.setString(1, "%" + keyword + "%");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Menu menu = new Menu(
                        result.getString("item"),
                        result.getDouble("price")
                );
                menu.setId(result.getInt("id"));
                list.add(menu);
            }

        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }

        return list;
    }

}
