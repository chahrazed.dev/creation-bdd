package co.groupproject;

public class Menu {

private Integer id;
private String item;
private Double price;

public Menu(Integer id, String item, Double price) {
    this.id = id;
    this.item = item;
    this.price = price;
}

public Menu(String item, Double price) {
    this.item = item;
    this.price = price;
}

public Menu() {
}

public Integer getId() {
    return id;
}

public void setId(Integer id) {
    this.id = id;
}

public String getItem() {
    return item;
}

public void setItem(String item) {
    this.item = item;
}

public Double getPrice() {
    return price;
}

public void setPrice(Double price) {
    this.price = price;
}

}
