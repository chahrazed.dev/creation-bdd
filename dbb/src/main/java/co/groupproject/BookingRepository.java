package co.groupproject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookingRepository {

    public List<Booking> findAllBookings() {
        List<Booking> list = new ArrayList<>();

        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM booking");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Booking booking = new Booking(
                        result.getTimestamp("date").toLocalDateTime(),
                        result.getString("name"),
                        result.getInt("phone"),
                        result.getString("special_needs")
                );

                booking.setId(result.getInt("id"));
                list.add(booking);
            }

        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }

        return list;
    }

    public Booking findBookingById(int id) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM booking WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            if (result.next()) {
                return new Booking(
                        result.getTimestamp("date").toLocalDateTime(),
                        result.getString("name"),
                        result.getInt("phone"),
                        result.getString("special_needs")
                );
            }

        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }

        return null;
    }

    public boolean deleteBooking(int id) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM booking WHERE id=?");
            stmt.setInt(1, id);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
            return false;
        }
    }

    public boolean persistBooking(Booking booking) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO booking (date, name, phone, special_needs) VALUES (?, ?, ?, ?)");
            stmt.setObject(1, booking.getDate());
            stmt.setString(2, booking.getName());
            stmt.setInt(3, booking.getPhone());
            stmt.setString(4, booking.getSpecialNeeds());
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
            return false;
        }
    }

    public boolean isBookingExists(int id) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT COUNT(*) AS count FROM booking WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return result.getInt("count") > 0;
            }
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }
        return false;
    }

    public boolean updateBookingFields(int id, String name, int phone, String specialNeeds) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE booking SET name=?, phone=?, special_needs=? WHERE id=?");
            stmt.setString(1, name);
            stmt.setInt(2, phone);
            stmt.setString(3, specialNeeds);
            stmt.setInt(4, id);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
            return false;
        }
    }

    
}
