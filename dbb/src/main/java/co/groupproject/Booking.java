package co.groupproject;


import java.time.LocalDateTime;

public class Booking {

    private int id;
    private LocalDateTime date;
    private String name;
    private Integer phone;
    private String specialNeeds;
    
    public Booking() {
    }

    public Booking(int id, LocalDateTime date, String name, Integer phone, String specialNeeds) {
        this.id = id;
        this.date = date;
        this.name = name;
        this.phone = phone;
        this.specialNeeds = specialNeeds;
    }

    public Booking(LocalDateTime date, String name, Integer phone, String specialNeeds) {
        this.date = date;
        this.name = name;
        this.phone = phone;
        this.specialNeeds = specialNeeds;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    public String getSpecialNeeds() {
        return specialNeeds;
    }

    public void setSpecialNeeds(String specialNeeds) {
        this.specialNeeds = specialNeeds;
    }

    
}
