import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import co.groupproject.Database;
import co.groupproject.Menu;
import co.groupproject.repository.MenuRepository;

public class MenuRepositoryTest {

    @BeforeEach
    void setUp() {
        try {
            ScriptRunner runner = new ScriptRunner(Database.connect());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    void shouldFindAllItems() {
        MenuRepository repo = new MenuRepository();
        List<Menu> result = repo.findAllitem();

        assertEquals(3, result.size());
        assertEquals("plat", result.get(0).getItem());
        assertEquals(19, result.get(0).getPrice());

    }

    @Test
    void shouldFindItemById() {
        MenuRepository repo = new MenuRepository();
        Menu result = repo.findItemById(1); 

        assertNotNull(result);
        assertEquals("plat", result.getItem());
        assertEquals(19, result.getPrice());

    }

    @Test
    void deleteSuccess() {
        MenuRepository repo = new MenuRepository();

        boolean result = repo.deleteItem(1);
        assertTrue(result);
        assertEquals(2, repo.findAllitem().size()); 
    }

    @Test
    void persistSuccess() {
        MenuRepository repo = new MenuRepository();
        Menu menu = new Menu("test item", 10.0); 
        assertTrue(repo.persistItem(menu));

    }

}
