    import static org.junit.jupiter.api.Assertions.*;

    import java.io.InputStreamReader;
    import java.sql.SQLException;
    import java.time.LocalDateTime;
    import java.util.List;
    
    import org.apache.ibatis.jdbc.ScriptRunner;
    import org.junit.jupiter.api.BeforeEach;
    import org.junit.jupiter.api.Test;
    
    import co.groupproject.Booking;
    import co.groupproject.Database;
    import co.groupproject.BookingRepository;
    
    public class BookingRepositoryTest {
    
        @BeforeEach
        void setUp() {
            try {
                ScriptRunner runner = new ScriptRunner(Database.connect());
                runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
            } catch (SQLException e) {
                e.printStackTrace();
            }
    
        }
    
        @Test
        void shouldFindAllBookings() {
            BookingRepository repo = new BookingRepository();
            List<Booking> result = repo.findAllBookings();
    
            assertEquals(3, result.size());
    /* 
            assertEquals(1, result.get(0).getId());
            assertEquals(LocalDateTime.of(2024, 6, 19, 0, 0), result.get(0).getDate());
            assertEquals("Hathat", result.get(0).getName());
            assertEquals(648751258, result.get(0).getPhone());
            assertEquals("non", result.get(0).getSpecialNeeds());*/
        }
    
        @Test
    void findByIdWithResult() {
        BookingRepository repo = new BookingRepository();
        Booking result = repo.findBookingById(1);

        assertEquals("Hathat", result.getName());
        
    }
    
        @Test
        void deleteSuccess() {
            BookingRepository repo = new BookingRepository();
            boolean result = repo.deleteBooking(1);
            assertTrue(result);   
            List<Booking> bookingsAfterDeletion = repo.findAllBookings();
            assertEquals(2, bookingsAfterDeletion.size());
        }
    
        @Test
        void persistSuccess() {
            BookingRepository repo = new BookingRepository();
            LocalDateTime date = LocalDateTime.now();
            Booking booking = new Booking(date, "Test Booking", 123456789, "test special needs");
    
            assertTrue(repo.persistBooking(booking));
    
            List<Booking> bookingsAfterPersist = repo.findAllBookings();
            assertEquals(4, bookingsAfterPersist.size()); 
        }
    
        @Test
        void isBookingExists() {
            BookingRepository repo = new BookingRepository();
            assertTrue(repo.isBookingExists(1)); 
            assertFalse(repo.isBookingExists(100));
        }
    
        @Test
        void updateBookingFields() {
            BookingRepository repo = new BookingRepository();
            assertTrue(repo.updateBookingFields(1, "Updated Name", 987654321, "Updated Special Needs"));
    
            Booking updatedBooking = repo.findBookingById(1);
            assertNotNull(updatedBooking);
            assertEquals("Updated Name", updatedBooking.getName());
            assertEquals(987654321, updatedBooking.getPhone());
            assertEquals("Updated Special Needs", updatedBooking.getSpecialNeeds());
        }
    }
    