import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import co.groupproject.Database;
import co.groupproject.entity.Table;
import co.groupproject.repository.TableRepository;

public class TableRepositoryTest {

    @BeforeEach
    void setUp() {
        try {
            ScriptRunner runner = new ScriptRunner(Database.connect());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    @Test
    void findAllShouldReturnAListOfTable() {
         TableRepository repo = new TableRepository();
        List<Table> result = repo.findAll();

        assertEquals(10, result.size());
        assertEquals(6, result.get(0).getCapacity());
        assertEquals(4, result.get(3).getTableNumber());

        assertNotNull(result.get(0).getCapacity());
        assertNotNull(result.get(0).getTableNumber());
        assertNotNull(result.get(0).getId());
    }

    @Test
    void findByIdWithResult() {
        TableRepository repo = new TableRepository();
        Table result = repo.findById(1);

        assertEquals(1, result.getTableNumber());
        assertEquals(6, result.getCapacity());
        assertEquals(1, result.getId());
    }

    @Test
    void findByIdNoResult() {
        TableRepository repo = new TableRepository();
        Table result = repo.findById(1000);

        assertNull(result);
    }

    @Test
    void updateSuccess() {
        TableRepository repo = new TableRepository();
        Table table = new Table ( 1,2, 5);
        assertTrue(repo.updateTable(table));

        Table updated = repo.findById(1);
        assertEquals(5, updated.getCapacity());
    }

    @Test
    void persistSuccess() {
        TableRepository repo = new TableRepository();
        Table table = new Table (20,2);
        boolean result = repo.persistTable(table);
        assertTrue(result);

        assertEquals(11, repo.findAll().size());
    }

    @Test
    void deleteSuccess() {
        TableRepository repo = new TableRepository();

        boolean result = repo.deleteTable(1);
        assertTrue(result);

        assertEquals(9, repo.findAll().size());
    }
    
}
