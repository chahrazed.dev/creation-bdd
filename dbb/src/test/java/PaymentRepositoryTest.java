import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import co.groupproject.Database;
import co.groupproject.entity.Payment;
import co.groupproject.repository.PaymentRepository;

public class PaymentRepositoryTest {

    @BeforeEach
    void setUp() {
        try {
            ScriptRunner runner = new ScriptRunner(Database.connect());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    void findAllShouldReturnAListOfPayment() {
        PaymentRepository repo = new PaymentRepository();
        List<Payment> result = repo.findAll();

        

        assertEquals(3, result.size());
        assertEquals("espece", result.get(0).getTypeOfPaiment());
        assertEquals("Yes", result.get(0).getPrintBill());
        assertEquals(1, result.get(0).getId());

                //Assertions moins précises mais qui marchent quelque soit ce qu'on a dans la bdd
        assertNotNull(result.get(0).getPrintBill());
        assertNotNull(result.get(0).getTypeOfPaiment());
        assertNotNull(result.get(1).getPrintBill());
        assertNotNull(result.get(1).getTypeOfPaiment());
    }

    @Test
    void findByIdWithResult() {
        PaymentRepository repo = new PaymentRepository();
        Payment result = repo.findById(2);

        assertEquals("No", result.getPrintBill());
        assertEquals("carte", result.getTypeOfPaiment());
        assertEquals(2, result.getId());
    }

    @Test
    void findByIdNoResult() {
        PaymentRepository repo = new PaymentRepository();
        Payment result = repo.findById(1000);

        assertNull(result);
    }

    @Test
    void updateSuccess() {
        PaymentRepository repo = new PaymentRepository();
        Payment paiement = new Payment ( 1,"carte", "No" );
        assertTrue(repo.updatePaiement(paiement));

        Payment updated = repo.findById(1);
        assertEquals("carte", updated.getTypeOfPaiment());
    }

    @Test
    void persistSuccess() {
        PaymentRepository repo = new PaymentRepository();
        Payment payment = new Payment ("oui","oui");
        boolean result = repo.persistPayment(payment);
        assertTrue(result);

        assertEquals(4, repo.findAll().size());
    }

    @Test
    void deleteSuccess() {
        PaymentRepository repo = new PaymentRepository();

        boolean result = repo.deletePaiement(1);
        assertTrue(result);

        assertEquals(2, repo.findAll().size());
    }
}
