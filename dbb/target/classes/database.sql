-- Active: 1709902741465@@127.0.0.1@3306@BDD
DROP TABLE IF EXISTS paiement;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS menu;
DROP TABLE IF EXISTS booking;
DROP TABLE IF EXISTS table_resto;


CREATE TABLE table_resto (
    id INT PRIMARY KEY AUTO_INCREMENT,
    table_number    INT NOT NULL,
    capacity    INT NOT NULL
);

CREATE TABLE booking (
    id INT PRIMARY KEY AUTO_INCREMENT,
    date DATETIME NOT NULL,
    name VARCHAR (255) NOT NULL,
    phone INT NOT NULL,
    special_needs VARCHAR (255),
    id_table_resto INT,
    Foreign Key (id_table_resto) REFERENCES table_resto(id)
);

CREATE TABLE menu (
    id INT PRIMARY KEY AUTO_INCREMENT,
    item VARCHAR(255),
    price INT
);

CREATE TABLE orders (
    id INT PRIMARY KEY AUTO_INCREMENT,
    id_table_resto INT,
    id_menu INT,
    Foreign Key (id_table_resto) REFERENCES table_resto(id),
    Foreign Key (id_menu) REFERENCES menu(id)
);

CREATE TABLE paiement(
    id INT PRIMARY KEY AUTO_INCREMENT,
    typeOfPaiement VARCHAR(255),
    printBill VARCHAR(255),
    id_orders INT,
    Foreign Key (id_orders) REFERENCES orders(id)
);

INSERT INTO table_resto (table_number, capacity) VALUES 
    (1,6),
    (2,4),
    (3,6),
    (4,2),
    (5,2),
    (6,8),
    (7,4),
    (8,4),
    (9,6),
    (10,2);


INSERT INTO booking (date,name,phone,special_needs,id_table_resto) VALUES
    ('2024-06-19', 'Hathat',0648751258,'non',5),
    ('2024-06-01', 'Matoiri',0658539524,'une chaise bébé',5),
    ('2024-06-25', 'Dieu',0645485564,'non',5);

INSERT INTO menu (item,price) VALUES
   ("plat", 19),("boisson", 2),("dessert", 5);


INSERT INTO paiement (typeOfPaiement, printBill) VALUES
   ("espece", "Yes"),("carte", "No"),("carte", "Yes");
