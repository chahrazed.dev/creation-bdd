# Projet de gestion de restaurant

Ce projet vise à concevoir la couche data d'une application pour un restaurant, permettant de gérer le menu et les commandes.

## Compétences visées

- Mettre en place une base de données relationnelle
- Développer des composants d'accès aux données SQL 


## Diagramme de Use Case

![Diagramme de Use Case](img/Projet-Groupe-USECASE.png)

## Maquettes fonctionnelles

![Accueil 1](img/Accueil_responsable.png)
![Accueil 2](img/Acceuil_Seerveurs.png)
![Accueil 3](img/Acceuil_Cuisiniers.png)
![Facture 4](img/Facture.png)
![Inventaire 5](img/Inventaire.png)
![Nouvelle résa 6](img/nouvelle_reservation.png)
![Commande 7](img/nv_commande.png)
![Menu 8](img/Menu.png)
![Resrvations 9](img/Reservations.png)
![Tables 10](img/Tables.png)


## Diagramme de classe

![Diagramme de classe](img/diagrammeClasses.png)

## Script SQL

Le script SQL de création des tables et d'insertion des données de test se trouve dans le fichier `database.sql`.

## Lien Trello 
https://trello.com/b/YuWENslZ/conception-bdd

N'hésitez pas à explorer le dépôt pour plus de détails.